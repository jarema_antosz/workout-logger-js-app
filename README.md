1. install node.js
2. Install global dependencies:
	$ npm install -g gulp bower
3. Install the local requirements:
	$ npm install
4. Install the Bower components:
	$ bower install
5. Run locally: 
	$ gulp
6. Create a build:
	$ gulp build