// gulp
var gulp = require('gulp');

// plugins
var connect = require('gulp-connect');
var jshint = require('gulp-jshint');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var minifyCSS = require('gulp-minify-css');
var clean = require('gulp-clean');
var runSequence = require('gulp-run-sequence');
var debug = require('gulp-debug');
var del = require('del');


// tasks
gulp.task('lint', function() {
  gulp.src(['./app/**/*.js', '!./app/bower_components/**'])
    .pipe(jshint())
    .pipe(jshint.reporter('default'))
    .pipe(jshint.reporter('fail'));
});
gulp.task('clean', function () {
  del.sync(['./app/js/bundled.js']);
  del.sync(['./dist/*']);
});

gulp.task('concat-scripts', function() {
  return gulp.src('./app/js/*.js')
      .pipe(concat('bundled.js'))
      .pipe(gulp.dest('./app/js'));
});
gulp.task('concat-scriptsDist', function() {
  return gulp.src('./app/js/*.js')
      .pipe(concat('bundled.js'))
      .pipe(uglify({
        // inSourceMap:
        // outSourceMap: "app.js.map"
      }))
      .pipe(gulp.dest('./dist/js'));
});
gulp.task('minify-css', function () {
  var opts = {comments: true, spare: true};
  gulp.src(['./app/**/*.css', '!./app/bower_components/**'])
      .pipe(minifyCSS(opts))
      .pipe(gulp.dest('./dist/'))
});
gulp.task('copy-bower-components', function () {
  gulp.src('./app/bower_components/**')
    .pipe(gulp.dest('dist/bower_components'));
});
gulp.task('copy-html-files', function () {
  gulp.src('./app/**/*.html')
    .pipe(gulp.dest('dist/'));
});
gulp.task('connect', function () {
  connect.server({
    root: 'app/',
    port: 8888
  });
});
gulp.task('connectDist', function () {
  connect.server({
    root: 'dist/',
    port: 9999
  });
});

// default task
gulp.task('default',function() {
  runSequence(
    'clean',
    'lint', 'concat-scripts', 'connect'
  );
}
);
gulp.task('build', function() {
  runSequence(
    ['clean'],
    ['lint', 'concat-scriptsDist'], ['minify-css', 'copy-html-files', 'copy-bower-components', 'connectDist']
  );
});